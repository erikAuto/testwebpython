import requests
import copy
import os


def execute_request(method, path, body, token, params=""):
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    response = requests.request(method=method, url=path, headers=headers, json=body, params=params, verify=True)
    return response


def replace(url, _list):
    for r in _list:
        url = url.replace(*r)
    return url


def walk_json(obj, keys):
    for key in keys:
        obj = obj[key]
    return obj


class AttrDict(dict):
    def __getattr__(self, item):
        return self[item]


def update(value, nvalue):
    if not isinstance(value, dict) or not isinstance(nvalue, dict):
        return nvalue
    for k, v in nvalue.items():
        value.setdefault(k, dict())
        if isinstance(v, dict):
            v = update(value[k], v)
        value[k] = v
    return value


def replace_value_with_definition(cant, _dict):
    for i in range(cant):
        new_body = copy.deepcopy(_dict)
        for key in _dict.keys():
            if 'behave' in str(_dict[key]):
                new_body[key] = f"""{_dict[key]}{i + 1}"""
    return _dict


def log_file(value):
    f = open("log.txt", "a+")
    f.write(value)
    f.close()


def clear_log():
    f = open("log.txt", "w")
    f.close()


def compare_ndic(src, dst, pre=''):
    for key, val in src.items():
        if pre:
            print_skey = pre + '.' + key
        else:
            print_skey = key
        if key not in dst.keys():
            print('Key "{}" in {} does not existed in {}'.format(print_skey, 'src', 'dst'))
        else:
            if isinstance(val, dict) and isinstance(dst.get(key), dict):
                # If the value of the same key is still dict
                compare_ndic(val, dst.get(key), print_skey)
            elif val == dst.get(key):
                print('Value of key "{}" in {} is the same with value in {}'.format(print_skey, 'src', 'dst'))
            else:
                print('Value of key "{}" in {} is different with value in {}'.format(print_skey, 'src', 'dst'))


def context_table_get(context, attr, default=None, index=0):
    return context.table[index][attr] if context.table is not None and attr in context.table[0] else default


def get_var_using_dict_path(object_, string_path, path_index=0):
    split_path = string_path.split('/')
    assert len(split_path) > 0, f'Unexpected length for split_path value: {len(split_path)}. Expected > 0'
    final_value = object_[split_path[path_index]]
    if path_index + 1 < len(split_path):
        return get_var_using_dict_path(final_value, string_path, path_index + 1)
    else:
        return final_value


def set_adaptative_field_value(body, i):
    new_body = body.copy()
    [new_body.__setitem__(
        key,
        f"{body[key]}{os.getenv('CLIENT_ID')}{str(i)}"
        if 'behave' in str(body[key]) else body[key]) for key in body.keys()]
    return new_body
