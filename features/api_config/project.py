from datetime import datetime

now = datetime.now()


def api_config_project(**kwargs):
    context = kwargs.get("context", None)
    API_CONFIG_DATA = {
        "project": {
            "url": "https://reqres.in/api/users",
            "body": {
                "name": "morpheus",
                "job": "leader"
            },
        }
    }

    return API_CONFIG_DATA
