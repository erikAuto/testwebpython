from datetime import datetime

now = datetime.now()
json_content = {}


def data_endpoints(context):
    from features.api_config.project import api_config_project

    return api_config_project(context=context)
