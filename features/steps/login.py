from behave import *
import os
import json
from features.api_config.data import *
from hamcrest import assert_that, equal_to, contains_exactly
from utils.functions import execute_request,log_file
from datetime import datetime

now = datetime.now()

@step('The user log in the API with correct credentials')
def send_login_request(context):
    if hasattr(context, 'token'):
        passcl
    else:
        body = data_endpoints(context)['project']['body']
        url = data_endpoints(context)['project']['url']
        req = execute_request("POST", url, body, None, None)
        context.response = req
        assert_that([req.status_code],contains_exactly(201))
        log_file(
            f"""system time: {str(now)}\r\nbody:{body}\r\nstatus code:{req.status_code}\r\nresponse:{req.text}\r\n"**************END LINE**************"\r\n""")
