import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from config import *


@step("The user opens amazon and enters the name of the article to search {data}")
def test_initially(context,data):
    data = data.replace("_"," ")
    print(data)
    dirpath = os.path.dirname(os.path.dirname(os.path.abspath(__file__))).replace("tests", "")
    print(dirpath)
    filepath = os.path.join(dirpath, 'chromedriver')
    driver = webdriver.Chrome(executable_path=filepath)
    driver.get("https://www.amazon.com/")
    driver.maximize_window()
    elem = driver.find_element_by_id("twotabsearchtextbox")
    elem.clear()
    elem.send_keys(data)
    elem.send_keys(Keys.RETURN)
    driver.implicitly_wait(10)
    driver.close()