import json


def update_json(json_name, field, value):
    with open("utils/json/" + json_name + ".json", 'r+') as f:
        json_data = json.load(f)
        json_data[field] = value
        f.seek(0)
        f.write(json.dumps(json_data))
        f.truncate()


def clean_data(json_name):
    with open("utils/json/" + json_name + ".json", 'r+') as f:
        json_data = json.load(f)
        for val in json_data:
            json_data[val] = ""
        f.seek(0)
        f.write(json.dumps(json_data))
        f.truncate()


def read_from_json(json_name, field):
    with open("utils/json/" + json_name + ".json") as f:
        json_data = json.load(f)
        data = json_data[field]
        f.close()
    return data

