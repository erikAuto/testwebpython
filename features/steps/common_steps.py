from behave import *
from utils.functions import clear_log


@step("The evidence in the log.txt file is cleaned up")
def cleaned_evidence(context):
    clear_log()
